const { Router } = require('express');
const router = Router();

const UsersController = require("../controllers/UsersController");
const ContentsController = require("../controllers/ContentsController");
const AuthController = require("../controllers/AuthController");

const adminMiddleware = require("../middlewares/admin");
const validator = require("../config/validator");

const passport = require("passport");

const path = require("path");

const multer = require("multer");
const storage = require("../config/files");
const upload = multer({

  storage: storage,

  fileFilter: function (request, file, callback) {

    const ext = path.extname(file.originalname);

    if (ext !== ".png" && ext !== ".jpg" && ext !== ".jpeg") {

      return callback(new Error("Extensão de arquivo inválida. Tente png, jpg ou jpeg!"), false);

    };

    callback(null, true);

  },

  limits: {

    fileSize: 2048 * 2048

  }

});

// Token generating routes.
router.post('/register', validator.validationUser('create'), AuthController.register);
router.post('/login', AuthController.login);

// Admin - Logged.
//router.use('/logged_admin', adminMiddleware);
router.post('/logged_admin/content/:id_admin', adminMiddleware, ContentsController.create);
router.put('/logged_admin/content/:id_content/:id_admin', adminMiddleware, ContentsController.update);
router.delete('/logged_admin/content/delete/:id_content/:id_admin', adminMiddleware, ContentsController.destroy);
router.get('/logged_admin/users/:id_admin', adminMiddleware, UsersController.index);
router.get('/logged_admin/users/:id_user/:id_admin', adminMiddleware, UsersController.show);
router.delete('/logged_admin/user/delete/:id_user/:id_admin', adminMiddleware, UsersController.destroy);

// User - Logged.
router.use("/logged_user", passport.authenticate('jwt', { session: false }));

router.put('/logged_user/update/:id', UsersController.update);
router.get('/logged_user/profile/:id', AuthController.getDetails);

router.put("/logged_user/add/profile_photo/:id", upload.single('profile_photo'), UsersController.addProfilePhoto);
router.put("/logged_user/delete/profile_photo/:id", UsersController.removeProfilePhoto);

router.put('/logged_user/like/content/:id', UsersController.addRelationContent);
router.put('/logged_user/unlike/content/:id', UsersController.removeRelationContent);

router.put('/logged_user/like/user/:id', UsersController.like);
router.put('/logged_user/unlike/user/:id', UsersController.unlike);

router.get('/logged_user/your/likes/:id', UsersController.yourLikes);
router.get('/logged_user/your/tanners/:id', UsersController.yourTanners);
router.get('/logged_user/your/five_stars_contents/:id', ContentsController.yourFiveStarsContent);

// User - Visitor.
router.get('/content', ContentsController.index);
router.get('/content/:id', ContentsController.show);

module.exports = router;