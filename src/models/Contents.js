const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Contents = sequelize.define('Contents', {

  title: {

    type: DataTypes.STRING,
    allowNull: false

  },

  description: {

    type: DataTypes.STRING,
    allowNull: false

  },

  age_rating: {

    type: DataTypes.INTEGER,
    allowNull: false

  },

  gender: {

    type: DataTypes.STRING,
    allowNull: false

  },

  type: {

    type: DataTypes.STRING,
    allowNull: false

  },

  star_count: {

    type: DataTypes.INTEGER

  },

  banner_photo: {

    type: DataTypes.STRING,

  },

},

  { timestamps: false }

);

Contents.associate = function (models) {

  Contents.belongsToMany(models.Users, {

    through: 'listFavorites',
    as: 'usersWhoLiked'

  });

};

module.exports = Contents;