const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Users = sequelize.define('Users', {

  name: {

    type: DataTypes.STRING,
    allowNull: false

  },

  email: {

    type: DataTypes.STRING,
    unique: true,
    allowNull: false

  },

  hash: {

    type: DataTypes.STRING,

  },

  salt: {

    type: DataTypes.STRING,

  },

  date_birth: {

    type: DataTypes.DATEONLY,
    allowNull: false

  },

  bio: {

    type: DataTypes.STRING

  },

  profile_photo: {

    type: DataTypes.STRING,

  },

  is_admin: {

    type: DataTypes.BOOLEAN

  }

},

  { timestamps: false }

);

Users.associate = function (models) {

  Users.belongsToMany(models.Users, {

    through: 'listMatches',
    as: 'yourLikes',
    foreignKey: 'tannerPerson'

  });

  Users.belongsToMany(models.Users, {

    through: 'listMatches',
    as: 'yourTanners',
    foreignKey: 'likedPerson'

  });

  Users.belongsToMany(models.Contents, {

    through: 'listFavorites',
    as: 'likeContent'

  });

};

module.exports = Users;