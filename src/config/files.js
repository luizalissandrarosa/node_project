const multer = require("multer");

const path = require("path");

const storage = multer.diskStorage({

  destination: path.join(__dirname, "..", "..", "uploads"),

  filename: function (request, file, callback) {

    callback(null, Date.now() + "-" + file.originalname);

  }

});

module.exports = storage;