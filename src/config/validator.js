const { body } = require("express-validator");

const validationUser = (method) => {

  switch (method) {

    case 'create': {

      return [

        body('name')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
        ,

        body('email')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
          .isEmail().withMessage('Preencha com um e-mail válido, por favor.')
        ,

        body('date_birth')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 1 }).withMessage('Por favor, preencha o campo.')
          .isDate().withMessage("Preencha do seguinte modo: YYYY/MM/DD")
        ,

        body('password')
          .exists().withMessage("Esse campo não pode ser vazio.")
          .isLength({ min: 8 }).withMessage('Por favor, preencha com no mínimo 8 caracteres.')

      ];

    };

  };

};

module.exports = {

  validationUser

};