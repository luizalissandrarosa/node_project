const User = require("../models/Users");

const admin = async (request, response, next) => {

  console.log(request.params)

  const { id_admin } = request.params;

  try {

    const user = await User.findByPk(id_admin);

    if (user.is_admin == 1) {

      return next();

    } else {

      return response.status(401).json({ 'error': 'Sem autorização.' });

    };

  } catch (error) {

    return response.status(500).json({ 'error': 'Internal Server Error.' });

  };

};

module.exports = admin;