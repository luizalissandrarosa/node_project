const Contents = require('../../models/Contents');
const faker = require('faker-br');

const seedContents = async function () {

  const contents = [];

  for (let id = 0; id < 10; id++) {

    contents.push({

      title: faker.name.title(),
      description: faker.lorem.paragraph(),
      age_rating: faker.random.number({
        'min': 0,
        'max': 18
      }),
      date_birth: faker.date.past(),
      gender: faker.lorem.words(),
      type: faker.lorem.word(),
      star_count: faker.random.number({
        'min': 0,
        'max': 5
      }),
      banner_photo: faker.image.imageUrl()

    });

  };

  try {

    await Contents.sync({ force: true });
    await Contents.bulkCreate(contents);

  } catch (err) {

    console.log(err);

  };

};

module.exports = seedContents;