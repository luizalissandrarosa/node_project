const Users = require('../../models/Users');
const Contents = require('../../models/Contents');

const faker = require('faker-br');

const seedUsers = async function () {

  try {

    await Users.sync({ force: true });

    for (let id = 0; id < 10; id++) {

      let user = await Users.create({

        email: faker.internet.email(),
        name: faker.name.firstName(),
        hash: faker.random.uuid(),
        salt: faker.random.uuid(),
        date_birth: faker.date.past(),
        is_admin: faker.random.boolean(),
        bio: faker.lorem.sentence(),
        profile_photo: faker.image.imageUrl()

      });

      if (id > 1) {

        let userLiked = await Users.findByPk(id - 1);

        user.addYourLikes(userLiked);

        let content = await Contents.findByPk(id - 1);

        await user.addLikeContent(content);

      };

    };

  } catch (err) {

    console.log(err);

  };

};

module.exports = seedUsers;