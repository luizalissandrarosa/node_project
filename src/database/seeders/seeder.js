require('../../config/dotenv')();
require('../../config/sequelize');

const seedContents = require('./ContentsSeeder');
const seedUsers = require('./UsersSeeder');

(async () => {

  try {

    await seedContents();
    await seedUsers();

  } catch (err) {

    console.log(err);

  }

})();
