const Contents = require('../models/Contents');

// Create content.

const create = async (request, response) => {

  try {

    const content = await Contents.create(request.body);

    return response.status(201).json({ message: "Conteúdo cadastrado com sucesso.", content: content });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List all.

const index = async (request, response) => {

  try {

    const contents = await Contents.findAll({

      include: [

        'usersWhoLiked'

      ]

    });

    return response.status(200).json({ contents });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List by Id.

const show = async (request, response) => {

  const { id_content } = request.params;

  try {

    const content = await Contents.findByPk(id_content, {

      include: [

        'usersWhoLiked'

      ]

    });

    return response.status(200).json({ content });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Lists a user's 5-star content.

const yourFiveStarsContent = async (request, response) => {

  try {

    const contents = await Contents.findAll({

      where: {

        star_count: 5

      }

    });

    return response.status(200).json({ contents });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Update.

const update = async (request, response) => {

  const { id_content } = request.params;

  try {

    const updated = await Contents.update(request.body, { where: { id: id_content } });

    if (updated) {

      const content = await Contents.findByPk(id_content);

      return response.status(200).send(content);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Conteúdo não encontrado.")

  };

};

// Delete.

const destroy = async (request, response) => {

  const { id_content } = request.params;

  try {

    const deleted = await Contents.destroy({ where: { id: id_content } });

    if (deleted) {

      return response.status(200).json("Conteúdo deletado com sucesso.");

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Conteúdo não encontrado.");

  };

};

module.exports = {

  index,
  show,
  yourFiveStarsContent,
  create,
  update,
  destroy

};