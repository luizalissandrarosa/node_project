const UserController = require("./UsersController");

const Auth = require("../config/auth");

const User = require("../models/Users");

const register = async (request, response) => {

  try {

    const user = await UserController.create(request, response);

    const token = Auth.generateJWT(user);

    return response.status(200).json({ user: user, token: token });

  } catch (error) {

    return response.status(500).json({ err: error });

  };

};

const login = async (request, response) => {

  try {

    const user = await User.findOne({ where: { email: request.body.email } });

    if (!user) {

      return response.status(404).json({ message: "Usuário não encontrado." });

    };

    const { password } = request.body;

    if (Auth.checkPassword(password, user.hash, user.salt)) {

      const token = Auth.generateJWT(user);

      return response.status(200).json({ token: token });

    } else {

      return response.status(401).json({ message: "Senha inválida." });

    };

  } catch (error) {

    return response.status(500).json({ err: error });

  };

};

const getDetails = async (request, response) => {

  try {

    const token = Auth.getToken(request);

    const payload = Auth.decodeJWT(token);

    const user = await User.findByPk(payload.sub);

    if (!user) {

      return response.status(404).json({ message: "Usuário não encontrado." });

    };

    return response.status(200).json({ user: user });

  } catch (error) {

    return response.status(500).json({ err: error });

  };

};

module.exports = {

  register,
  login,
  getDetails

};