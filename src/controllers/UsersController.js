const User = require('../models/Users');
const Contents = require('../models/Contents');

const path = require("path");

const Auth = require("../config/auth");
const mailer = require("../config/mail").mailer;

const { validationResult } = require('express-validator');

const fsPromises = require("fs").promises;

const handlebars = require("handlebars");
const readHTML = require("../config/mail").readHTMLFile;

// Create user.

const create = async (request, response) => {

  try {

    validationResult(request).throw();

    const { password } = request.body;

    const hashAndSalt = Auth.generatePassword(password);

    const salt = hashAndSalt.salt;
    const hash = hashAndSalt.hash;

    const listAdmins = ["luiza@gmail.com", "george@gmail.com"];

    let verificationAdmin = false;

    if (listAdmins.includes(request.body.email)) {

      verificationAdmin = true;

    };

    const newUserData = {

      name: request.body.name,
      email: request.body.email,
      date_birth: request.body.date_birth,
      bio: request.body.bio,
      profile_photo: request.body.profile_photo,
      is_admin: verificationAdmin,
      hash: hash,
      salt: salt

    };

    const user = await User.create(newUserData);

    const pathTemplate = path.resolve(__dirname, "..", "..", "templates");

    readHTML(path.join(pathTemplate, "main.html"), (err, html) => {

      const template = handlebars.compile(html);

      const replacements = {

        name: user.name

      };

      const htmlToSend = template(replacements);

      const message = {

        from: "testesEJCM@gmail.com",
        to: user.email,
        subject: "Apenas um teste",
        html: htmlToSend

      };

      mailer.sendMail(message);

    });

    return user;

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List all.

const index = async (request, response) => {

  try {

    const users = await User.findAll({

      include: [

        'yourTanners',
        'yourLikes',
        'likeContent'

      ]

    });

    return response.status(200).json({ users });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List by Id.

const show = async (request, response) => {

  const { id_user } = request.params;

  try {

    const user = await User.findByPk(id_user, {

      include: [

        'yourTanners',
        'yourLikes',
        'likeContent'

      ]

    });

    return response.status(200).json({ user });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Update.

const update = async (request, response) => {

  const { id } = request.params;

  try {

    const updated = await User.update(request.body, { where: { id: id } });

    if (updated) {

      const user = await User.findByPk(id);

      return response.status(200).send(user);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

// Delete.

const destroy = async (request, response) => {

  const { id_user } = request.params;

  try {

    const deleted = await User.destroy({ where: { id: id_user } });

    if (deleted) {

      return response.status(200).json("Usuário deletado com sucesso.");

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.");

  };

};

// Adding user-content relationship.

const addRelationContent = async (request, response) => {

  const { id } = request.params;

  try {

    let user = await User.findByPk(id);

    const content = await Contents.findByPk(request.body.ContentId);

    await user.addLikeContent(content);

    user = await User.findByPk(id, {

      include: [

        'likeContent'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Removing user-content relationship.

const removeRelationContent = async (request, response) => {

  const { id } = request.params;

  try {

    let user = await User.findByPk(id);

    const content = await Contents.findByPk(request.body.ContentId);

    await user.removeLikeContent(content);

    user = await User.findByPk(id, {

      include: [

        'likeContent'

      ]

    });

    return response.status(200).json(user);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Like people.

const like = async (request, response) => {

  const { id } = request.params;

  try {

    let userLike = await User.findByPk(id);

    const userLiked = await User.findByPk(request.body.userId);

    await userLike.addYourLikes(userLiked);

    userLike = await User.findByPk(id, {

      include: "yourLikes"

    });

    return response.status(200).json(userLike);

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Unlike people.

const unlike = async (request, response) => {

  const { id } = request.params;

  try {

    let userLike = await User.findByPk(id);

    const userLiked = await User.findByPk(request.body.userId);

    await userLike.removeYourLikes(userLiked);

    userLike = await User.findByPk(id, {

      include: [

        'yourLikes'

      ]

    });

    return response.status(200).json(userLike);

  } catch (err) {

    return response.status(500).json({ error: err });

  }

};

// List of people you liked.

const yourLikes = async (request, response) => {

  const { id } = request.params;

  try {

    const user = await User.findByPk(id);

    const listYourLikes = await user.getYourLikes();

    return response.status(200).json({ listYourLikes });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// List of people who like you.

const yourTanners = async (request, response) => {

  const { id } = request.params;

  try {

    const user = await User.findByPk(id);

    const listYourTanners = await user.getYourTanners();

    return response.status(200).json({ listYourTanners });

  } catch (err) {

    return response.status(500).json({ error: err });

  };

};

// Add profile photo.

const addProfilePhoto = async (request, response) => {

  const { id } = request.params;

  const path = process.env.APP_URL + "/uploads/" + request.file.filename;

  try {

    const updated = await User.update({ profile_photo: path }, { where: { id: id } });

    if (updated) {

      const user = await User.findByPk(id);

      return response.status(200).send(user);

    };

    throw new Error();

  } catch (err) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

// Remove profile photo.

const removeProfilePhoto = async (request, response) => {

  try {

    const { id } = request.params;

    const user = await User.findByPk(id);

    const photo = user.profile_photo;

    const pathDb = photo.split("/").slice(-1)[0];

    await fsPromises.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));

    await User.update({ profile_photo: null }, { where: { id: id } });

    const userNew = await User.findByPk(id);

    return response.status(200).send(userNew);

  } catch (error) {

    return response.status(500).json("Usuário não encontrado.")

  };

};

module.exports = {

  index,
  show,
  create,
  update,
  destroy,
  addRelationContent,
  removeRelationContent,
  like,
  unlike,
  yourTanners,
  yourLikes,
  addProfilePhoto,
  removeProfilePhoto

};
