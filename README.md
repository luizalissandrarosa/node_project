<div align="center" id="top">

# Node project #

</div>

&#xa0;

<div align="center">

<p>
  <a href="#dart-about"> About </a> &#xa0; | &#xa0;
  <a href="#game_die-database-modeling"> Database modeling </a> &#xa0; | &#xa0;  
  <a href="#computer-technologies-used"> Technologies used </a> &#xa0; | &#xa0; 
  <a href="#white_check_mark-requirements"> Requirements </a> &#xa0; | &#xa0; 
  <a href="#checkered_flag-starting"> Starting </a> &#xa0; | &#xa0; 
  <a href="#bulb-roadmap"> Roadmap </a> &#xa0; | &#xa0;
  <a href="#unlock-license"> License </a> &#xa0; | &#xa0;
  <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Developer </a>
</p>

</div>

<br>

## :dart: About ##

This is the backend of the <a href="https://gitlab.com/luizalissandrarosa/zoetrope-landingpage">Zoetrope</a> project and was proposed in the <a href='https://ejcm.com.br/'> EJCM </a> selection process.

:warning: In construction

## :game_die: Database modeling ##

Database modeling done in <a href="https://github.com/chcandido/brModelo">brModelo<a/>.

<div align="center">

<img src="database_modeling_english.png" />

</div>

## :computer: Technologies used ##

- [Node](https://nodejs.org/en/)
- [Sequelize](https://sequelize.org/)
- [SQLite3](https://www.sqlite.org/index.html)
- [Express](https://expressjs.com/pt-br/)
- [Nodemon](https://www.npmjs.com/package/nodemon)
- [Faker BR](https://www.npmjs.com/package/faker-br?activeTab=readme)

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) and [Node](https://nodejs.org/en/) installed.

## :checkered_flag: Starting ##

```bash
# Clone the folder.
$ git clone https://gitlab.com/luizalissandrarosa/node_project

# Access it.
$ cd node_project

# Install the dependencies.
$ npm i

# Run the migrations.
$ npm run migrate

# Run the project.
$ npm start
```

- To test the application's routes, <a href='https://insomnia.rest/'> Insomnia </a> was used. 
Use the configuration I used through the button below:

[![Run in Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=Zoetrope&uri=https%3A%2F%2Fgitlab.com%2Fluizalissandrarosa%2Fnode_project%2F-%2Fraw%2Fmain%2Finsomnia.json)

## :bulb: Roadmap ##

- [x] Add users auto-relationship
- [x] Add like and dislike functionality for users
- [x] List the likes and likes of users
- [ ] Make sure that if two users like each other, they match

- [x] Add relationship between users and contents
- [x] Add like and dislike functionality for movies

## :unlock: License ##

This project is under license from MIT. To learn more, see [LICENSE](LICENSE). 

&#xa0;

<hr/>

Developed with ❤ by <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Luiza Lissandra :rocket: </a>

<a href="#top"> Back to top </a>
